# Flask settings
FLASK_DEBUG = True  # Do not use debug mode in production

# Flask-Restplus settings
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE                 = True
RESTPLUS_MASK_SWAGGER             = False
RESTPLUS_ERROR_404_HELP           = False

# SQLAlchemy settings
SQLALCHEMY_DATABASE_URI        = 'mysql+pymysql://emsn:emsn@database/emsn'  #'sqlite:///db.sqlite' #   db.sqlite   :memory:
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_POOL_SIZE           = 5
SQLALCHEMY_POOL_RECYCLE        = 120
SQLALCHEMY_MAX_OVERFLOW        = 100
SQLALCHEMY_ECHO                = False
