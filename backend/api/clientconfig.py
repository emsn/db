import logging
from   flask                   import request
from   flask_restplus          import Resource, abort
from   backend.api.serializers import serializer_client_config
from   backend.api.restplus    import api
from   backend.api.parsers     import config_arguments
from   backend.database.models import GroundStation
from   backend.database        import db

log = logging.getLogger(__name__)

ns = api.namespace('clientconfig', description='get client config')


@ns.route('/')
class GetClientConfig(Resource):

    @api.expect(config_arguments)
    @api.marshal_with(serializer_client_config)
    def get(self):
        """
        Get client configuration
        """
        log.info('Sending client configuration')
        args = config_arguments.parse_args(request)
        log.debug(args)
        id      = args.get('station_id')
        api_key = args.get('api_key')
        gs      = db.session.query(GroundStation).filter_by(id = id).filter_by(api_key = api_key).first()

        if gs == None:
            log.info("Wrong security data")
            abort(401, "Wrong groundstation_id or api_key")

        log.debug(gs)
        return gs
