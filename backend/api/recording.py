import logging
from   sys                     import maxsize
from   datetime                import datetime
from   flask                   import request
from   flask_restplus          import Resource, reqparse
from   backend.api.business    import add_recording
from   backend.api.serializers import serialize_recording
from   backend.api.restplus    import api
from   backend.database.models import Recording
from   backend.database        import db
from   backend.database.models import GroundStation, Detection, Recording

log = logging.getLogger(__name__)

ns = api.namespace('recording', description='update an recording')

#parser.add_argument('date', type=lambda x: datetime.strptime(x,'%Y-%m-%dT%H:%M:%S'))
recording_arguments = reqparse.RequestParser()
recording_arguments.add_argument('start', type=lambda x: datetime.strptime(x,'%Y-%m-%dT%H:%M:%S'), required = False, help = 'first date for the query' )
recording_arguments.add_argument('end',   type=lambda x: datetime.strptime(x,'%Y-%m-%dT%H:%M:%S'), required = False, help = 'last date for the query'  )
recording_arguments.add_argument('low',   type = int,                                              required = False, help = 'lowest frequency'         )
recording_arguments.add_argument('high',  type = int,                                              required = False, help = 'hightest frequency'       )
recording_arguments.add_argument('gid',   type = int,                                              required = False, help = 'Ground station id'        )

def clean_recording_arguments(args):
    if args['start'] is None:
        args['start'] = datetime(year=2000, month = 1, day = 1, hour = 0, minute = 0, second = 0)
    if args['end'] is None:
        args['end'] = datetime.now()
    if args['low'] is None:
        args['low'] = 0
    if args['high'] is None:
        args['high'] = maxsize
    return args


@ns.route('/')
class RecordingCollection(Resource):

    @api.response(201, 'recording successfully uploaded.')
    @api.expect(serialize_recording)
    def post(self):
        """
        Uploads an recording
        """
        log.info('Syntactic valid post recording received')
        data = request.json
        add_recording(data)
        return None, 201

    @api.response(201, "ok")
    @api.expect(recording_arguments, validate = True)
    def get(self):
        """
        Returns recorded data
        """
        args = clean_recording_arguments(recording_arguments.parse_args())
        log.debug(args)

        # distinct takes ages to return, therefore without distinct and list(set(result)) at the end
        if args['gid'] is None:
            log.debug("Searching without gid")
            r = db.session.query(Recording.recording_date.label("date"), Recording.frequency.label("frequency"), Recording.recording_db.label("db")).\
                filter(Recording.recording_date >= args['start']).\
                filter(Recording.recording_date <= args['end']).\
                filter(Recording.frequency      >= args['low']).\
                filter(Recording.frequency      <= args['high']).\
                order_by(Recording.recording_date, Recording.frequency).\
                all()
        else:
            log.debug('Search with gid')
            r = db.session.query(Recording.recording_date.label("date"), Recording.frequency.label("frequency"), Recording.recording_db.label("db")).\
                filter(Recording.recording_date   >= args['start']).\
                filter(Recording.recording_date   <= args['end']).\
                filter(Recording.frequency        >= args['low']).\
                filter(Recording.frequency        <= args['high']).\
                filter(Recording.groundstation_id == args['gid']).\
                order_by(Recording.groundstation_id).\
                all()

        log.info("Done")
        return [[row.date.strftime("%Y-%m-%dT%H:%M:%S"), row.frequency, row.db] for row in r], 201


@ns.route('/getGID')
class GetGID(Resource):

    @api.expect(recording_arguments, validate = True)
    def get(self):
        """
        Returns either all GIDS or selected ones, depending on param
        """
        args = clean_recording_arguments(recording_arguments.parse_args())
        log.debug(args)

        # distinct takes ages to return, therefore without distinct and list(set(result)) at the end
        if args['gid'] is None:
            log.debug("Searching without gid")
            r = db.session.query(Recording.groundstation_id.label("groundstation")).\
                filter(Recording.recording_date >= args['start']).\
                filter(Recording.recording_date <= args['end']).\
                filter(Recording.frequency      >= args['low']).\
                filter(Recording.frequency      <= args['high']).\
                order_by(Recording.groundstation_id).\
                all()
        else:
            log.debug('Search with gid')
            r = db.session.query(Recording.groundstation_id.label("groundstation")).\
                filter(Recording.recording_date   >= args['start']).\
                filter(Recording.recording_date   <= args['end']).\
                filter(Recording.frequency        >= args['low']).\
                filter(Recording.frequency        <= args['high']).\
                filter(Recording.groundstation_id == args['gid']).\
                order_by(Recording.groundstation_id).\
                all()

        return list(set([row.groundstation for row in r])), 201


@ns.route('/getFrequency')
class GetFrequency(Resource):

    @api.expect(recording_arguments, validate = True)
    def get(self):
        """
        Returns recorded frequencies in a given time slot
        """
        args = clean_recording_arguments(recording_arguments.parse_args())
        log.debug(args)

        if args['gid'] is None:
            log.debug("Searching without gid")
            r = db.session.query(Recording.frequency.distinct().label("frequency")).\
                filter(Recording.recording_date >= args['start']).\
                filter(Recording.recording_date <= args['end']).\
                filter(Recording.frequency      >= args['low']).\
                filter(Recording.frequency      <= args['high']).\
                order_by(Recording.frequency).\
                all()
        else:
            log.debug('Search with gid')
            r = db.session.query(Recording.frequency.distinct().label("frequency")).\
                filter(Recording.recording_date   >= args['start']).\
                filter(Recording.recording_date   <= args['end']).\
                filter(Recording.frequency        >= args['low']).\
                filter(Recording.frequency        <= args['high']).\
                filter(Recording.groundstation_id == args['gid']).\
                order_by(Recording.frequency).\
                all()

        return [row.frequency for row in r], 201


@ns.route('/getTime')
class GetTime(Resource):

    @api.expect(recording_arguments, validate = True)
    def get(self):
        """
        Returns recorded time slots for a given Frequency
        """
        args = clean_recording_arguments(recording_arguments.parse_args())
        log.debug(args)

        if args['gid'] is None:
            log.debug("Searching without gid")
            r = db.session.query(Recording.recording_date.distinct().label("date")).\
                filter(Recording.recording_date >= args['start']).\
                filter(Recording.recording_date <= args['end']).\
                filter(Recording.frequency      >= args['low']).\
                filter(Recording.frequency      <= args['high']).\
                order_by(Recording.recording_date).\
                all()
        else:
            log.debug('Search with gid')
            r = db.session.query(Recording.recording_date.distinct().label("date")).\
                filter(Recording.recording_date   >= args['start']).\
                filter(Recording.recording_date   <= args['end']).\
                filter(Recording.frequency        >= args['low']).\
                filter(Recording.frequency        <= args['high']).\
                filter(Recording.groundstation_id == args['gid']).\
                order_by(Recording.recording_date).\
                all()

        return [row.date.strftime("%Y-%m-%dT%H:%M:%S") for row in r], 201
