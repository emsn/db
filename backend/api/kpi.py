import logging
from   flask                   import request
from   flask_restplus          import Resource, abort
from   backend.api.serializers import serialize_kpi
from   backend.api.restplus    import api
from   backend.database        import db
from   backend.database.models import GroundStation, Detection, Recording

log = logging.getLogger(__name__)

ns = api.namespace('kpis', description='get kpis config')


@ns.route('/')
class GetKPIs(Resource):

    @api.marshal_with(serialize_kpi)
    def get(self):
        """
        Get KPIs
        """
        log.info('Sending KPIs')
        rval = {}

        rval['stations'] = db.session.query(GroundStation).count()
        rval['meteors']  = db.session.query(Detection).count()
        rval['datasets'] = db.session.query(Recording).group_by(Recording.recording_date).group_by(Recording.groundstation_id).count()

        return rval
