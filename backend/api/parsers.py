from flask_restplus import reqparse

config_arguments = reqparse.RequestParser()
config_arguments.add_argument('station_id' , type = int, required = True, help = 'Station ID'  )
config_arguments.add_argument('api_key'    , type = str, required = True, help = 'Station Key' )