import logging
import datetime
import _thread
from   flask                   import current_app
from   dateutil                import parser
from   backend.database        import db
from   backend.database.models import User, Recording, GroundStation
from   flask_restplus          import abort

log = logging.getLogger(__name__)

def persist_recording(data, app):
    frequency_low    = int(   data.get('frequency_low')    )
    frequency_hight  = int(   data.get('frequency_high')   )
    chain            = float( data.get('chain')            )
    recording        =        data.get('recording')
    temperature      = float( data.get('temperature')      )
    groundstation_id = int(   data.get('groundstation_id') )

    log.debug("Thread started")

    with app.app_context():
        session = db.session
        counter = 0

        for row in recording.split("\n"):

            if len(row) > 0:
                val = row.split(",")
                recording_date = datetime.datetime(2020, 1, 1) + datetime.timedelta(seconds=int(val[0]))
                frequency = frequency_low

                for v in val[1:]:

                    if v != "-inf" or v != "inf":
                        try:
                            recording_db = float(v)                 #v-- this is a dummy, need to fix the client
                            recording    = Recording(recording_date, 1, round(frequency), recording_db, groundstation_id)
                            frequency   += chain
                            session.add(recording)
                        except ValueError:
                            log.info("Value is not a float: %s" % v)

            counter += 1

            if counter > 100:
                session.commit()

        session.commit()
        session.close()

    log.debug("Thread end")

def add_recording(data):

    groundstation_id = int(   data.get('groundstation_id') )
    key              =        data.get('api_key')

    #log.debug(data)
    gs = db.session.query(GroundStation).filter_by(id = groundstation_id).filter_by(api_key = key).first()

    if gs == None:
        log.info("Wrong security data")
        abort(401, "Wrong groundstation_id or api_key")

    log.info("Groundstation found: %s" % gs)
    _thread.start_new_thread(persist_recording, (data, current_app._get_current_object(), ))
