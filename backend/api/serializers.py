from flask_restplus       import fields
from backend.api.restplus import api

serialize_recording = api.model('recording', {
    'groundstation_id': fields.Integer (required = True,                                         description = 'Groundstation_id'                    ),
    'api_key':          fields.String  (required = True,  min_length = 23, max_length = 23,      description = 'Groundstation API Key'               ),
    'temperature':      fields.Float   (required = False, min = -20,       max = 120,            description = 'Raspberry Pi Core Temperature'       ),
    'frequency_low':    fields.Integer (required = True,  min = 10,        max = 2000000000,     description = 'Lowest Frequecy of the scan in Hz'   ),
    'frequency_high':   fields.Integer (required = True,  min = 10,        max = 2000000000,     description = 'Highest Frequency of the scan in Hz' ),
    'chain':            fields.Float   (required = True,  min = 1,         max = 10000,          description = 'Chain size in Hz'                    ),
    'recording':        fields.String  (required = True,  min_length = 10, max_length = 5000000, description = 'The recording itself as csv data'    )
})

serializer_client_config = api.model('groundstation',{
    'frequency_low':    fields.Integer (required = True, min = 10, max = 1000000000, description = 'Lower Frequency range' ),
    'frequency_high':   fields.Integer (required = True, min = 10, max = 1000000000, description = 'High Frequency range'  ),
    'frequency_chain':  fields.Integer (required = True, min = 1,  max = 100000,     description = 'Chain Frequency size'  ),
    'gain':             fields.Integer (required = True, min = 1,  max = 100,        description = 'Gain level'            ),
    'integration_time': fields.Integer (required = True, min = 1,  max = 100,        description = 'Time to integrate'     ),
    'time_to_run':      fields.Integer (required = True, min = 10, max = 10000000,   description = 'Time to run'           )
})

serialize_kpi = api.model('kpi',{
    'stations': fields.Integer (required = True, min = 0, description = 'How many stations are registered'),
    'meteors':  fields.Integer (required = True, min = 0, description = 'How many meteors detected'       ),
    'datasets': fields.Integer (required = True, min = 0, description = 'How many hours ovservations'     )
})
