from datetime         import datetime
from backend.database import db


class User(db.Model):
    __tablename__  =           'users'
    id             = db.Column('id',       db.Integer,    nullable = False, unique = True,  index = False, primary_key = True)
    username       = db.Column('username', db.String(80), nullable = False, unique = True,  index = False                    )
    joined         = db.Column('joined',   db.DateTime,   nullable = False, unique = False, index = False                    )

    def __init__(self, username, joined = None):
        self.username = username

        if joined is None:
            joined = datetime.utcnow()

        self.joined = joined

    def __repr__(self):
        return '<Post %r>' % self.username


class GroundStation(db.Model):
    __tablename__    =           'groundstations'
    id               = db.Column('id',               db.Integer,    nullable = False, unique = True,  index = False, primary_key = True )
    name             = db.Column('name',             db.String(80), nullable = False, unique = True,  index = False                     )
    api_key          = db.Column('api_key',          db.String(23), nullable = False, unique = True,  index = False                     )
    station_owner_id = db.Column('station_owner_id', db.Integer,    db.ForeignKey('users.id'),        index = False                     )
    longitude        = db.Column('longitude',        db.Float,      nullable = False, unique = False, index = False                     )
    latitude         = db.Column('latitude',         db.Float,      nullable = False, unique = False, index = False                     )
    elevation        = db.Column('elevation',        db.Float,      nullable = False, unique = False, index = False                     )
    frequency_low    = db.Column('frequency_low',    db.Integer,    nullable = False, unique = False, index = False                     )
    frequency_high   = db.Column('frequency_high',   db.Integer,    nullable = False, unique = False, index = False                     )
    frequency_chain  = db.Column('frequency_chain',  db.Integer,    nullable = False, unique = False, index = False                     )
    gain             = db.Column('gain',             db.Integer,    nullable = False, unique = False, index = False                     )
    integration_time = db.Column('integration',      db.Integer,    nullable = False, unique = False, index = False                     )
    time_to_run      = db.Column('time_to_run',      db.Integer,    nullable = False, unique = False, index = False                     )
    temperature      = db.Column('temperature',      db.Float,      nullable = True,  unique = False, index = False                     )
    created          = db.Column('created',          db.DateTime,   nullable = False, unique = False, index = False                     )
    # station_owner    = db.relationship("User", backref=db.backref('groundstations', lazy='dynamic'))

    def __init__(self, name, api_key, station_owner_id,
                       longitude        = 12,
                       latitude         = 13,
                       elevation        = 0,
                       frequency_low    = 144000000,
                       frequency_high   = 146000000,
                       frequency_chain  = 150,
                       gain             = 50,
                       integration_time = 1,
                       time_to_run      = 120,
                       temperature      = 23,
                       created          = None):
        self.name             = name
        self.api_key          = api_key
        self.station_owner_id = station_owner_id
        self.longitude        = longitude
        self.latitude         = latitude
        self.elevation        = elevation
        self.frequency_low    = frequency_low
        self.frequency_high   = frequency_high
        self.frequency_chain  = frequency_chain
        self.gain             = gain
        self.integration_time = integration_time
        self.time_to_run      = time_to_run
        self.temperature      = temperature

        if created is None:
            created = datetime.utcnow()

        self.created = created

    def __repr__(self):
        return '<name %r>' % self.name


class Recording(db.Model):
    __tablename__    =           'recordings'
    id               = db.Column('id',               db.Integer,  nullable = False, unique = True,    index = False, primary_key = True )
    recording_date   = db.Column('recording_date',   db.DateTime, nullable = False, unique = False,   index = False                     )
    recording_length = db.Column('recording_length', db.Integer,  nullable = False, unique = False,   index = False                     )
    frequency        = db.Column('recording',        db.Integer,  nullable = False, unique = False,   index = False                     )
    recording_db     = db.Column('db',               db.Float,    nullable = False, unique = False,   index = False                     )
    groundstation_id = db.Column('groundstation_id', db.Integer,  db.ForeignKey('groundstations.id'), index = False                     )
    # groundstation    = db.relationship("Groundstation", backref=db.backref('recordings', lazy='dynamic'))

    def __init__(self, recording_date, recording_length, frequency, recording_db, groundstation_id):
        self.recording_date   = recording_date
        self.recording_length = recording_length
        self.frequency        = frequency
        self.recording_db     = recording_db
        self.groundstation_id = groundstation_id

    def __repr__(self):
        return '<frequency %r>' % self.frequency

class Detection(db.Model):
    __tablename__    = 'detection'
    id               = db.Column('id',               db.Integer,  nullable = False, unique = True,    index = False, primary_key = True )
    detection_date   = db.Column('detection_date',   db.DateTime, nullable = False, unique = False,   index = False                     )
    groundstation_id = db.Column('groundstation_id', db.Integer,  db.ForeignKey('groundstations.id'), index = False                     )

    def __init__(self, detection_date, groundstation_id):
        self.detection_date   = detection_date
        self.groundstation_id = groundstation_id

    def __repr__(self):
        return 'detection date %r' % self.detection_date
