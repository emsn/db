from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

def reset_database():
    from backend.database.models import User, GroundStation, Recording, Detection
    db.drop_all()
    db.create_all()

    user = User('ansi')
    groundstation = GroundStation('Horzizon', '12345678901234567890123', 1)
    db.session.add(user)
    db.session.add(groundstation)
    db.session.commit()
