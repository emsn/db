import os
import logging.config
from   flask                    import Flask, Blueprint
from   backend                  import settings
from   backend.api.recording    import ns as recording_namespace
from   backend.api.clientconfig import ns as client_config
from   backend.api.kpi          import ns as kpis
from   backend.api.restplus     import api
from   backend.database         import db, reset_database
from   flask_limiter            import Limiter
from   flask_limiter.util       import get_remote_address
from   flask_openid             import OpenID
from   flask_cors               import CORS
from   werkzeug.contrib.fixers  import ProxyFix

app          = Flask  (__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
CORS(app)
limiter = Limiter(app, key_func=get_remote_address, default_limits=["1500 per day", "100 per hour"])
oid     = OpenID (app, 'oidstore', safe_roots=[])

logging_conf_path = os.path.normpath(os.path.join(os.path.dirname(__file__), 'backend/logging.conf'))
logging.config.fileConfig(logging_conf_path, disable_existing_loggers = False)
log = logging.getLogger(__name__)

app.config['SWAGGER_UI_DOC_EXPANSION']       = settings.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
app.config['RESTPLUS_VALIDATE']              = settings.RESTPLUS_VALIDATE
app.config['RESTPLUS_MASK_SWAGGER'      ]    = settings.RESTPLUS_MASK_SWAGGER
app.config['ERROR_404_HELP']                 = settings.RESTPLUS_ERROR_404_HELP

app.config['SQLALCHEMY_DATABASE_URI']        = settings.SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = settings.SQLALCHEMY_TRACK_MODIFICATIONS
app.config['SQLALCHEMY_POOL_SIZE']           = settings.SQLALCHEMY_POOL_SIZE
app.config['SQLALCHEMY_POOL_RECYCLE']        = settings.SQLALCHEMY_POOL_RECYCLE
app.config['SQLALCHEMY_MAX_OVERFLOW']        = settings.SQLALCHEMY_MAX_OVERFLOW
app.config['SQLALCHEMY_ECHO']                = settings.SQLALCHEMY_ECHO

blueprint = Blueprint('api', __name__, url_prefix='/v0')
api.init_app(blueprint)
api.add_namespace(recording_namespace)
api.add_namespace(client_config)
api.add_namespace(kpis)
app.register_blueprint(blueprint)
db.init_app(app)

if os.path.isdir("new"):
    log.info("Resetting DB")
    with app.app_context():
        reset_database()


if __name__ == "__main__":
   app.run(host='0.0.0.0', port=8888, debug=settings.FLASK_DEBUG)
