# db

[![pipeline status](https://gitlab.com/emsn/db/badges/master/pipeline.svg)](https://gitlab.com/emsn/db/commits/master)

## Development
```
docker-compose stop api && docker-compose rm -f a pi && docker-compose pull api && docker-compose up -d api && docker-compose logs -f api
```

HDSDR
SDRPlayDriversettings.png

LO   0143.050.000
Tune 0143.049.900

Bandwidth: 12000Hz Output

# Design

## www
1. Documentation
2. How to participate
3. Press / Sponsor information
4. KPI

## Interactive
1.See meteorit stats
  1.Per day / week / month / year
  2.Size / duration information
2.See waterfall
  1.Select by frequency (low/high), time(start/end), station (gid)
3.See locations
  1.Stations
  2.Meteorits, select time range
4.Configure own station
  1.Recording Time
    1. Lowest frequence
    2. Highest Frequence
    3. Start time
    4. End time
    5. Amplification
    6. Resolution
    7. Repeat schudule (cron like?)
  2.Picture
  3.Lat, Long, Elevation
  4.Description
  5.Read Only
     1.Temperature chart / current temperature Raspi CPU
     2.Statistik uptime / processing hours
     3.Live view collected data (Sexy Dashboard)
5.Configure own account
  1.Profile picture
  2.Name
  3.Email (stats)
  4.Telegram (meteorite notifications / stats)
  5.Login Credentials (OpenID)
  6.Description
6.Stats
  1.KPIs
    1.#Users
    2.#Detected Meteorits
      1.Small
      2.Medium
      3.Big
    3.#Data Units
    4.#Hours of Observations

## API
1.For Clients
2.For Interactive Page
3.For OpenData Access
  1.Meteorit findings
  2.Raw Access
