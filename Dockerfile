FROM python:latest

ADD  backend/requirements.txt requirements.txt
RUN  pip install -r requirements.txt

COPY app.py app.py
COPY backend backend

CMD  gunicorn -w 1 -b 0.0.0.0:8000 -t 500 app:app
