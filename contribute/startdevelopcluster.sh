# Create namespace
kubectl apply -f namespace.yaml
kubectl apply -f bluemix.yaml
kubectl apply -f secret.yaml

# services
kubectl apply -f mariadb-deployment.yaml
kubectl apply -f redis-depolyment.yaml

# db
kubectl apply -f db-deployment.yaml
